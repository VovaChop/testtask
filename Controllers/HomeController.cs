﻿using System;
using System.Collections.Generic;
using System.Net.Mime;
using System.Web.Mvc;
using System.Xml;
using TestTask.Models;
using TestTask;

namespace TestTask.Controllers
{
    public class HomeController : Controller
    {
        private Models.SomeDBEntities1 db = new SomeDBEntities1();

        // GET: Home
        public ActionResult Index()
        {
            AddDataToDB("..\XmlFiles\UsersXML.xml");
            var Items = db.Users;
            //var Items = LoadDataFromXML();
            return View(Items);
        }

        /// <summary>
        /// Generate data for DB from XML file
        /// <param name="path">Path to XML file</param>
        /// </summary>
        [NonAction]
        private void AddDataToDB(string path)
        {
            List<User> users = LoadDataFromXml(path);
            foreach (User user in users)
            {
                db.Users.Add(user);
            }
            db.SaveChanges();
        }

        [NonAction]
        /// <summary>
        /// Generate list of users from XML file
        /// </summary>
        /// <param name="path">Path to XML file</param>
        /// <returns></returns>
        private List<User> LoadDataFromXml(string path)
        {
            XmlDocument xDoc = new XmlDocument();
            //xDoc.Load(@"F:\UsersXML.xml");
            xDoc.Load(path);

            XmlElement xRoot = xDoc.DocumentElement;

            List<User> users = new List<User>();

            foreach (XmlNode node in xRoot)
            {
                User user = new User();
                XmlNode attr = node.Attributes.GetNamedItem("id");
                if (attr != null)
                {
                    user.Id = Int32.Parse(attr.Value);
                }

                foreach (XmlNode childNode in node.ChildNodes)
                {
                    if (childNode.Name == "name")
                        user.Name = childNode.InnerText;
                    if (childNode.Name == "lastname")
                        user.LastName = childNode.InnerText;
                    if (childNode.Name == "phone")
                        user.Phone = childNode.InnerText;
                    if (childNode.Name == "email")
                        user.Email = childNode.InnerText;
                    if (childNode.Name == "birthday")
                        user.Birthday = DateTime.Parse(childNode.InnerText);
                }

                users.Add(user);
            }

            return users;
        }
    }
}